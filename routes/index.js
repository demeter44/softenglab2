var express = require('express');
var router = express.Router();

const util = require('util');
const mysql = require('mysql');


const connection = mysql.createConnection({
  host: 'softengdb',
  user: 'root',
  password: 'root',
  database: 'softengdb'
})

const query = util.promisify(connection.query).bind(connection);

/* GET home page. */
router.get('/', async function(req, res) {
	const dbLines = await getLinesFromDB();
  res.render('index', { title: 'Express', lines: dbLines });
});

module.exports = router;

async function getLinesFromDB(){
	const result = await query("select * from softengtable");
	result.forEach((val)=>{
		console.log(val.testvaluevarchar);
	})
	return result;
}
