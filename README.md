# softenglab2

# dockerhub:

https://hub.docker.com/repository/docker/demeter44/softeng2

# SQL scripts to run:

create database softengdb;
use softengdb;
create table softengtable(
testvalue varchar(100)
);
insert into softengtable values ('val1');
insert into softengtable values ('val2');
select * from softengtable;